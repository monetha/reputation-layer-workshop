# reputation-layer-workshop

An boilerplate application is available at `boilerplate` branch

Full implementation is available at `master` branch

Documents with private keys of fact providers are available on `boilerplate` branch inside `/docs` folder


Further documentation abour reputation-js-sdk and it's usage can be found [here](https://github.com/monetha/reputation-js-sdk)

