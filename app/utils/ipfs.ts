import { IIPFSClient, IIPFSDag } from 'verifiable-data';
import { INFURA_IPFS_API_URL } from 'app/const';

export class IIPFSClientReader implements IIPFSClient {
  public dag: IIPFSDag;

  public cat = async(path: string): Promise<any> => {
    const response = await fetch(`${INFURA_IPFS_API_URL}/cat?arg=${path}`);
    if (!response.ok) {
      const errMsg = await response.text();
      throw new Error(errMsg);
    }

    return response.blob();
  }

  public add = () => {
    throw new Error('not implemented');
  }
}

export const getIPFSClient = async () => {
  return new IIPFSClientReader();
};
