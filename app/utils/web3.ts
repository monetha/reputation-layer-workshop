import { convertCallbackToPromise } from './promise';
import { ETH_NETWORK_URL } from 'app/const';
import Web3 from 'web3';

export enum BlockStatuses {
  Failed = '0x0',
  Passed = '0x1',
}

let web3: Web3;

export function getWeb3() {
  if (web3) {
    return web3;
  }

  web3 = new Web3(new Web3.providers.HttpProvider(ETH_NETWORK_URL));

  if (!web3.eth.net.isListening()) {
    throw new Error('Ethereum node address is incorrect');
  }

  return web3;
}

/**
 * waitForTxToFinish waits for transaction to finish for the given txHash,
 * returns a promise which is resolved when transaction finishes.
 * @param {string} txHash a string with transaction hash as value
 */
export const waitForTxToFinish = (txHash: string): Promise<any> =>
  new Promise((resolve, reject) => {

    const waiter = async () => {
      try {
        let result = await getTransactionReceipt(txHash);

        if (result) {
          resolve(result);
          return;
        }
      }
      catch (err) {
        reject(err);
        return;
      }

      setTimeout(waiter, 1000);
    }

    waiter();
  });

export const getTransactionReceipt = (txHash: string) => {
  const web3 = getWeb3();

  return convertCallbackToPromise(
    web3.eth.getTransactionReceipt,
    txHash,
  );
}
