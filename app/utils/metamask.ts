import { TransactionConfig } from 'web3-core';
import { convertCallbackToPromise } from './promise';

/**
 * Enables metamask usage so it can be used or throws error otherwise
 */
export const enableMetamask = async () => {
  const ethereum = getMetamaskEthereumInstance();

  if (!ethereum) {
    throw new Error('Please install metamask chrome extension');
  }

  if (!ethereum.selectedAddress) {
    throw new Error('Please select an account in metamask');
  }

  try {
    const [currentAddress] = await ethereum.enable();
    if (!currentAddress) {
      throw new Error('Unknown reason');
    }
  } catch (e) {
    console.error(e);

    let msg;
    if (typeof e === 'string') {
      msg = e;
    } else {
      msg = e.message;
    }

    throw new Error('Could not enable metamask: ' + msg);
  }
}

/**
 * Gets current account address
 */
export const getCurrentAccountAddress = () => {
  const ethereum = getMetamaskEthereumInstance();

  if (!ethereum || !ethereum.selectedAddress) {
    return null;
  }

  return ethereum.selectedAddress;
}

/**
 * Submits transaction using metamask and returns hash
 */
export const sendTransaction = async (rawTx: TransactionConfig): Promise<string> => {
  const ethereum = getMetamaskEthereumInstance();
  if (!ethereum) {
    throw new Error('Metamask is not enabled');
  }

  const { from, to, data } = rawTx;

  return convertCallbackToPromise(
    ethereum.sendAsync,
    {
      method: 'eth_sendTransaction',
      params: [{
        from,
        to,
        data,
      }],
      from: ethereum.selectedAddress,
    }
  ) as Promise<string>;
}

export const getMetamaskEthereumInstance = () => {
  return (window as any).ethereum;
}