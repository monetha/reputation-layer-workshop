import { ICategorizedFacts, clapCategory } from './categories';

const factProviderWeights = {
  '0x59d019A7d24E88bA29C738c97dAD6Eb3a27da990': 10,
  '0x5AcAf9893d6f283dcad2378B238927271d29006A': 10,
  '0x3e71B553D86ea0f5f4640444Eef7C1be139EF01C': 10,
  '0x551fC637Dc1e777223F387E08A4eF06F36aC13c8': 3,
  '0x6d8D47b587a0F761ecF6aFb95d83f60CF98A044E': 3,
  '0xAdfeD83D3BC9d01570b3CBB75513bdf81180084e': 3,
  '0xf1383c08ae7af182C11Bbb8E0271c31e93b0B279': 3,
  '0x837a61C99688eBbbE4eC0D6860e9156Aae6fc7FD': 3,
  '0xabCA87cF27FCC415f57E52F6da663Bbc4aaF249e': 3,
  '0xD57179d9685Ec1A74ABCD3FBB151FadbAAcAe9e2': 1,
  '0xDf0a49E55b6fAfF06122b977D7d8F9E796964ED9': 1,
  '0x221BA9F7898A23EB0b0Ad5429f90A4f4962A7332': 1,
  '0x9228d45454f606f4098b2B77795E738eaCc735d4': 2,
  '0x42800183dB22652862E36E2AFb42EEA1C12BaEE6': 1,
  '0x9710E1f8e9461bdD97E27D991074ddb9a789dD74': 2,
  '0xfdf82F5eC854B44b5a3100618d0FcD683d60CFc7': 2,
  '0x914242E37d6Bd5Af5a0255C6014ffFCC5Bb11FcA': 2,
  '0x9275b0Cb4A6B23c2c48B4a72E12B5d73C4f99D01': 1,
};

/**
 * Calculates social rating based on given facts and their credibility
 */
export const calculateSocialRating = (categorizedFacts: ICategorizedFacts) => {
  if (!categorizedFacts) {
    return 0;
  }

  let rating = 0;

  for (const categoryKey in categorizedFacts) {
    if (!categorizedFacts.hasOwnProperty(categoryKey) || categoryKey === clapCategory) {
      continue;
    }

    const facts = categorizedFacts[categoryKey];

    facts.forEach(fact => {
      rating += getProviderWeight(fact.factProviderAddress);
    });
  }

  return rating;
}

export const calculateClaps = (categorizedFacts: ICategorizedFacts) => {
  if (!categorizedFacts || !categorizedFacts[clapCategory]) {
    return 0;
  }

  const claps = categorizedFacts[clapCategory];
  const votedProviders = {};

  let count = 0;
  claps.forEach(clap => {

    // Allow only one vote for provider
    if (votedProviders[clap.factProviderAddress]) {
      return;
    }

    votedProviders[clap.factProviderAddress] = true;
    count++;
  })

  return count;
}

export const getProviderWeight = (providerAddress: string) => {
  const weight = factProviderWeights[providerAddress];
  if (weight === undefined) {
    return 1;
  }

  return weight;
}
