import { PASSPORT_OWNER_ADDRESS } from 'app/const';
import { getIPFSClient } from 'app/utils/ipfs';
import { getWeb3 } from 'app/utils/web3';
import { FactReader } from 'verifiable-data';
import { categorizeFacts, ICategorizedFacts } from './categories';
import { getStringFactRefs } from './facts';
import { calculateSocialRating } from './rating';

export interface ICandidateRef {
  fullName: string;
  passportAddress: string;
}

export interface ICandidate {
  fullName: string;
  passportAddress: string;
  photo: any;
  socialRating: number;
  categorizedFacts: ICategorizedFacts;
}

export const candidateRefs: ICandidateRef[] = [
  {
    fullName: 'Ingrida Šimonytė',
    passportAddress: '0xc417039C3367193DD6F4684cbf96646f47104CED',
  },
  {
    fullName: 'Gitanas Nausėda',
    passportAddress: '0xab8CF023d22c48f360bb4F3903DB53e86AE2a759',
  },
  {
    fullName: 'Saulius Skvernelis',
    passportAddress: '0xf9E02dFF3906611E51BB9d79805130767bA2ceA0',
  },
];

export const loadCandidateData = async (candidatePassportAddress: string): Promise<ICandidate> => {

  const candidate: Partial<ICandidate> = {
    passportAddress: candidatePassportAddress,
  };

  const web3 = getWeb3();
  const reader = new FactReader(web3, candidatePassportAddress);

  // Get name
  candidate.fullName = await reader.getString(PASSPORT_OWNER_ADDRESS, 'FullName');

  // Get photo
  const ipfsClient = await getIPFSClient();
  const photo: Blob = await reader.getIPFSData(PASSPORT_OWNER_ADDRESS, 'Photo', ipfsClient);

  candidate.photo = window.URL.createObjectURL(photo);

  // Get all facts for the passport (without fact data yet)
  const factRefs = await getStringFactRefs(candidatePassportAddress);
  candidate.categorizedFacts = categorizeFacts(factRefs);

  // Calculate rating
  candidate.socialRating = calculateSocialRating(candidate.categorizedFacts);

  return candidate as ICandidate;
};
