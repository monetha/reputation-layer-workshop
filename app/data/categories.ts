import { IFact } from 'app/data/facts';

export interface ICategory {
  title: string;
  key: string;
}

export interface ICategorizedFacts {
  [categoryKey: string]: IFact[];
}

export const categories: ICategory[] = [
  {
    title: 'Education',
    key: 'education',
  },
  {
    title: 'Occupation',
    key: 'occupation',
  },
  {
    title: 'Family status',
    key: 'family_status',
  },
  {
    title: 'Place of birth',
    key: 'birth_place',
  },
  {
    title: 'Work experience',
    key: 'work_experience',
  },
];

export const clapCategory = 'clap';

const keyExtractRegex = /(.*)_[^_]*$/gm;

/**
 * Groups facts to categories and omits unrecognized facts
 */
export const categorizeFacts = (facts: IFact[]) => {
  const validCategories = {
    [clapCategory]: true,
  };

  categories.forEach(category => validCategories[category.key] = true);

  const groups: ICategorizedFacts = {};

  facts.forEach(fact => {
    const result = keyExtractRegex.exec(fact.key);
    keyExtractRegex.lastIndex = 0;
    if (!result || !result[1]) {
      return;
    }

    const categoryKey = result[1];

    if (!validCategories[categoryKey]) {
      return;
    }

    if (!groups[categoryKey]) {
      groups[categoryKey] = [];
    }

    groups[categoryKey].push(fact);
  });

  return groups;
};
