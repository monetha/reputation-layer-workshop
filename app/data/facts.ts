import { FactWriter, FactReader, PassportReader, EventType, IHistoryEvent } from 'verifiable-data';
import { enableMetamask, getCurrentAccountAddress, sendTransaction } from 'app/utils/metamask';
import { getWeb3, waitForTxToFinish, BlockStatuses } from 'app/utils/web3';

export interface IFactPayload {
  sourceUrl: string;
  excerpt: string;
}

export interface IFact extends IFactPayload, IHistoryEvent  {}

/**
 * Submits fact about candidatee to candidatees passsport
 */
export const submitFact = async (candidatePassportAddress: string, factKey: string, factPayload: IFactPayload) => {

  // Ensure metamask is available and enabled
  await enableMetamask();

  const factProviderAddress = getCurrentAccountAddress();
  const factPayloadJSON = JSON.stringify(factPayload);

  // Generate TX
  const writer = new FactWriter(getWeb3(), candidatePassportAddress);
  const rawTX = await writer.setString(generateUniqueFactKey(factKey), factPayloadJSON, factProviderAddress);

  // Submit TX
  const txHash = await sendTransaction(rawTX);
  const txResult = await waitForTxToFinish(txHash);

  if (txResult.status === BlockStatuses.Failed) {
    console.error(txResult);
    throw new Error('Transaction has failed');
  }

  return txResult;
};

const generateUniqueFactKey = (factKeyPrefix) => {
  return `${factKeyPrefix}_${(new Date()).getTime()}`;
};

/**
 * Reads fact data from candidate passport
 */
export const getFactData = async (candidatePassportAddress: string, factKey: string, providerAddress: string) => {
  const reader = new FactReader(getWeb3(), candidatePassportAddress);
  const payload = await reader.getString(providerAddress, factKey);

  return JSON.parse(payload) as IFactPayload;
};

/**
 * Gets all string fact references (without fact contents) for given passport
 */
export const getStringFactRefs = async (passportAddress: string) => {
  const reader = new PassportReader(getWeb3());
  const events = await reader.readPassportHistory(passportAddress);

  const facts = events
    .filter(e => e.eventType === EventType.Updated)
    .map(e => ({
      ...e,
      sourceUrl: null,
      excerpt: null,
    } as IFact));

  return facts;
};
