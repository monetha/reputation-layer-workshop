import React from 'react';

// Style
import 'app/style/index.scss';
import { PageTemplate } from '../PageTemplate';
import { FactsForm } from '../FactsForm';
import { PassportList } from '../PassportList';

// #region -------------- Interfaces -------------------------------------------------------------------

interface IState {
  reloadRequestId: string;
}

// #endregion

// #region -------------- App -------------------------------------------------------------------

export default class App extends React.Component<any, IState> {
  constructor(props) {
    super(props);

    this.state = {
      reloadRequestId: 'initial',
    }
  }

  public render() {
    const { reloadRequestId } = this.state;

    return (
      <div className='app'>
        <PageTemplate>
          <div className='content-container'>
            <h1>Candidate profiles</h1>
            <PassportList reloadRequestId={reloadRequestId} />
          </div>

          <div className='content-container form-section'>
            <h1>Submit info about a candidate</h1>
            <FactsForm onFactSubmitted={this.onFactSubmitted} />
          </div>
        </PageTemplate>
      </div>
    );
  }

  private onFactSubmitted = () => {
    this.setState({
      ...this.state,
      reloadRequestId: (new Date()).getTime().toString(),
    });
  }
}

// #endregion
