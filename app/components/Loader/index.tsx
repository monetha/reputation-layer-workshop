import React from 'react';

export const Loader: React.SFC = () => {

  return (
    <div className='loader-container'>
      <div className='loader' />
    </div>
  );
}