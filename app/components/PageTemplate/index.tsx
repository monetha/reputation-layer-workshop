import React from 'react';

export const PageTemplate: React.SFC = ({ children }) => {

  return (
    <div className='page-template'>
      <div className='content'>
        <div>
          {children}
        </div>
      </div>
    </div>
  );
}