import { ICandidate } from 'app/data/candidates';
import React, { Fragment } from 'react';
import { IFact, submitFact } from 'app/data/facts';
import { categories, ICategory, clapCategory } from 'app/data/categories';
import { getProviderWeight, calculateClaps } from 'app/data/rating';

// #region -------------- Interfaces --------------------------------------------------------------

interface IProps {
  candidate: ICandidate;
  onRequestFactData(passportAddress: string, fact: IFact);
  onChanged(passportAddress: string);
}

interface IState {
  showFactViewer: boolean;
  isClapSubmitting: boolean;
}

// #endregion

// #region -------------- Component ---------------------------------------------------------------

export class Passport extends React.PureComponent<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = this.getInitialState();
  }

  public render() {

    return (
      <div className='passport'>
        {this.renderPhoto()}

        <div className='passport-info'>
          {this.renderName()}
          {this.renderClap()}
          {this.renderRating()}
          {this.renderFactViewerToggle()}
          {this.renderFactViewer()}
        </div>
      </div>
    );
  }

  private getInitialState = () => {
    return {
      showFactViewer: false,
      isClapSubmitting: false,
    };
  }

  // #region -------------- Name -------------------------------------------------------------------

  private renderName = () => {
    const { candidate } = this.props;

    let name = 'Candidate';

    if (candidate && candidate.fullName) {
      name = candidate.fullName;
    }

    return (
      <div className='name'>
        {name}
      </div>
    );
  }

  // #endregion

  // #region -------------- Clap -------------------------------------------------------------------

  private renderClap = () => {
    const { candidate } = this.props;
    if (!candidate || !candidate.categorizedFacts) {
      return null;
    }

    const count = calculateClaps(candidate.categorizedFacts);

    const { isClapSubmitting } = this.state;

    return (
      <div
        className={`claps ${isClapSubmitting ? 'is-submitting' : ''}`}
        onClick={this.onClap}
      >
        {count} {count === 1 ? 'clap' : 'claps'}
      </div>
    );
  }

  private onClap = async () => {
    const { onChanged, candidate } = this.props;
    const { isClapSubmitting } = this.state;

    if (isClapSubmitting) {
      return;
    }

    try {
      this.setState({
        ...this.state,
        isClapSubmitting: true,
      });

      await submitFact(candidate.passportAddress, clapCategory, null);
    } catch (err) {
      console.error(err);
      return;
    } finally {
      this.setState({
        ...this.state,
        isClapSubmitting: false,
      });
    }

    if (onChanged) {
      onChanged(candidate.passportAddress);
    }
  }

  // #endregion

  // #region -------------- Rating -------------------------------------------------------------------

  private renderRating = () => {
    const { candidate } = this.props;

    let rating = '...';

    if (candidate && candidate.socialRating !== null && candidate.socialRating !== undefined) {
      rating = candidate.socialRating.toString();
    }

    return (
      <div className='social-rating'>
        <div className='label'>Social rating</div>
        <div className='value'>{rating}</div>
      </div>
    );
  }

  // #endregion

  // #region -------------- Photo -------------------------------------------------------------------

  private renderPhoto = () => {
    const { candidate } = this.props;

    let imageUrl = null;

    if (candidate && candidate.photo) {
      imageUrl = candidate.photo;
    }

    return (
      <div
        className='candidate-photo'
        style={imageUrl && {
          backgroundImage: `url(${candidate.photo})`,
        }}
      >
      </div>
    );
  }

  // #endregion

  // #region -------------- Fact viewer -------------------------------------------------------------------

  private renderFactViewerToggle = () => {
    const { showFactViewer } = this.state;

    let text;
    if (showFactViewer) {
      text = 'Hide facts';
    } else {
      text = 'Show facts';
    }

    return (
      <a className='fact-viewer-toggle' href='#' onClick={this.onToggleFactViewer}>{text}</a>
    );
  }

  private onToggleFactViewer = (e) => {
    e.preventDefault();

    this.setState({
      ...this.state,
      showFactViewer: !this.state.showFactViewer,
    });
  }

  private renderFactViewer = () => {
    const { showFactViewer } = this.state;

    if (!showFactViewer) {
      return null;
    }

    const { candidate } = this.props;

    if (!candidate || !candidate.categorizedFacts) {
      return (
        <div className='fact-viewer'>
          No facts
        </div>
      );
    }

    const { categorizedFacts } = candidate;

    return (
      <div className='fact-viewer'>
        {
          categories.map(category => {
            if (!categorizedFacts[category.key]) {
              return null;
            }

            return (
              <Fragment key={category.key}>
                {this.renderFactCategory(category, categorizedFacts[category.key])}
              </Fragment>
            );
          })
        }
      </div>
    );
  }

  private renderFactCategory = (category: ICategory, facts: IFact[]) => {
    return (
      <div className='fact-category'>
        <div className='category-title'>{category.title}</div>

        {facts.map(fact => (
          <Fragment key={fact.key}>
            {this.renderFact(fact)}
          </Fragment>
        ))}
      </div>
    );
  }

  private renderFact = (fact: IFact) => {
    return (
      <div className='fact'>
        <div className='provider-address'>{fact.factProviderAddress}</div>
        <div className='weight'>Rating {getProviderWeight(fact.factProviderAddress)}</div>
        {this.renderFactContent(fact)}
      </div>
    );
  }

  private renderFactContent = (fact: IFact) => {
    const { onRequestFactData, candidate } = this.props;

    if (!fact.excerpt || !fact.sourceUrl) {
      if (onRequestFactData) {
        onRequestFactData(candidate.passportAddress, fact);
      }

      return (
        <div>...</div>
      );
    }

    let url = fact.sourceUrl;
    if (url.indexOf('://') === -1) {
      url = `http://${url}`;
    }

    return (
      <Fragment>
        <div className='excerpt'>{fact.excerpt}</div>
        <a
          target='blank'
          href={url}
          className='source-url'
        >
          {fact.sourceUrl}
        </a>
      </Fragment>
    );
  }

  // #endregion
}

// #endregion
