import React from 'react';
import { ICandidateRef, candidateRefs } from 'app/data/candidates';
import { ICategory, categories } from 'app/data/categories';
import { Loader } from '../Loader';
import { submitFact } from 'app/data/facts';

// #region -------------- Interfaces --------------------------------------------------------------

interface IProps {
  onFactSubmitted();
}

interface IState {
  selectedCandidate: ICandidateRef;
  selectedCategory: ICategory;
  sourceUrl: string;
  excerpt: string;
  error: string;
  isLoading: boolean;
}

// #endregion

// #region -------------- Component ---------------------------------------------------------------

export class FactsForm extends React.PureComponent<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = this.getInitialState();
  }

  public render() {
    return (
      <div className='facts-form'>
        <form onSubmit={this.onSubmit}>
          {this.renderCandidateDropdown()}
          {this.renderCategoryDropdown()}
          {this.renderSourceUrl()}
          {this.renderExcerpt()}

          {this.renderError()}

          {this.renderSubmitButton()}
          {this.renderLoader()}
        </form>
      </div>
    );
  }

  private getInitialState = () => {
    return {
      selectedCandidate: null,
      selectedCategory: null,
      sourceUrl: '',
      excerpt: '',
      error: null,
      isLoading: false,
    }
  }

  // #region -------------- Candidate -------------------------------------------------------------------

  private renderCandidateDropdown = () => {
    const { selectedCandidate } = this.state;

    return (
      <label>
        <div className='label'>Candidate</div>
        <select
          value={selectedCandidate ? selectedCandidate.passportAddress : ''}
          onChange={this.onCandidateChange}
        >
          {this.getCandidateOptions()}
        </select>
      </label>
    );
  }

  private getCandidateOptions = () => {
    const options = candidateRefs.map(candidate => (
      <option
        key={candidate.passportAddress}
        value={candidate.passportAddress}
      >
        {candidate.fullName}
      </option>
    ));

    if (!this.state.selectedCandidate) {
      options.unshift(this.getEmptyOption());
    }

    return options;
  }

  private onCandidateChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const passportAddress = e.target.value;
    const selectedCandidate = candidateRefs.find(c => c.passportAddress === passportAddress);

    this.setState({
      ...this.state,
      selectedCandidate,
    });
  }

  // #endregion

  // #region -------------- Category -------------------------------------------------------------------

  private renderCategoryDropdown = () => {
    const { selectedCategory } = this.state;

    return (
      <label>
        <div className='label'>Fact category</div>
        <select
          value={selectedCategory ? selectedCategory.key : ''}
          onChange={this.onCategoryChange}
        >
          {this.getCategoryOptions()}
        </select>
      </label>
    );
  }

  private getCategoryOptions = () => {
    const options = categories.map(category => (
      <option
        key={category.key}
        value={category.key}
      >
        {category.title}
      </option>
    ));

    if (!this.state.selectedCategory) {
      options.unshift(this.getEmptyOption());
    }

    return options;
  }

  private onCategoryChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const categoryKey = e.target.value;
    const selectedCategory = categories.find(c => c.key === categoryKey);

    this.setState({
      ...this.state,
      selectedCategory,
    });
  }

  // #endregion

  // #region -------------- Source url -------------------------------------------------------------------

  private renderSourceUrl = () => {
    const { sourceUrl } = this.state;

    return (
      <label>
        <div className='label'>Fact source URL</div>
        <input
          type='text'
          value={sourceUrl}
          onChange={this.onSourceUrlChange}
        />
      </label>
    );
  }

  private onSourceUrlChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const sourceUrl = e.target.value;

    this.setState({
      ...this.state,
      sourceUrl,
    });
  }

  // #endregion

  // #region -------------- Excerpt -------------------------------------------------------------------

  private renderExcerpt = () => {
    const { excerpt } = this.state;

    return (
      <label>
        <div className='label'>Fact excerpt</div>
        <textarea
          value={excerpt}
          onChange={this.onExcerptChange}
        />
      </label>
    );
  }

  private onExcerptChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const excerpt = e.target.value;

    this.setState({
      ...this.state,
      excerpt,
    });
  }

  // #endregion

  // #region -------------- Loader -------------------------------------------------------------------

  private renderLoader = () => {
    const { isLoading } = this.state;

    if (!isLoading) {
      return null;
    }

    return (
      <Loader />
    );
  }

  // #endregion

  // #region -------------- Error -------------------------------------------------------------------

  private renderError = () => {
    const { error } = this.state;

    if (!error) {
      return null;
    }

    return (
      <div className='msg-error'>{error}</div>
    )
  }

  // #endregion

  // #region -------------- Submit -------------------------------------------------------------------

  private renderSubmitButton = () => {
    return (
      <div>
        <button
          type='submit'
          className='btn btn-primary'
        >
          Submit
      </button>
      </div>
    );
  }

  private onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    // Validate form
    let error = this.validateForm();
    if (error) {
      this.updateState({ error });
      return;
    }

    // Clear errors, enable loader
    this.updateState({ error: null, isLoading: true });

    try {
      const { selectedCandidate, selectedCategory, excerpt, sourceUrl } = this.state;

      await submitFact(selectedCandidate.passportAddress, selectedCategory.key, {
        excerpt,
        sourceUrl,
      });
    } catch (err) {
      this.updateState({ error: err.message });
      return;
    } finally {
      this.updateState({ isLoading: false })
    }

    const { onFactSubmitted } = this.props;
    if (onFactSubmitted) {
      onFactSubmitted();
    }

    // Reset form
    this.setState(this.getInitialState());
  }

  private validateForm = () => {
    const { selectedCandidate, selectedCategory, excerpt, sourceUrl } = this.state;

    if (!selectedCandidate || !selectedCategory || !excerpt.trim() || !sourceUrl.trim()) {
      return 'All fields must be filled';
    }

    return null;
  }

  private updateState(updatedProps: Partial<IState>) {
    this.setState({
      ...this.state,
      ...updatedProps,
    });
  }

  // #endregion

  // #region -------------- Helpers -------------------------------------------------------------------

  private getEmptyOption = () => {
    return (
      <option
        key='empty'
        value=''
      >
        Choose...
      </option>
    )
  }

  // #endregion
}

// #endregion
