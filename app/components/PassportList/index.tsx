import { candidateRefs, ICandidate, loadCandidateData } from 'app/data/candidates';
import React from 'react';
import { Col, Row } from 'react-flexbox-grid';
import { Loader } from '../Loader';
import { Passport } from '../Passport';
import { getFactData, IFact } from 'app/data/facts';

// #region -------------- Interfaces --------------------------------------------------------------

interface IProps {
  reloadRequestId?: string;
}

interface IState {
  candidates: ICandidate[];
  error: string;
  isLoading: boolean;
}

// #endregion

// #region -------------- Component ---------------------------------------------------------------

export class PassportList extends React.PureComponent<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = this.getInitialState();
  }

  public componentDidMount() {
    this.loadCandidates();
  }

  public componentDidUpdate(prevProps) {
    if (prevProps.reloadRequestId !== this.props.reloadRequestId) {
      this.loadCandidates();
    }
  }

  public render() {
    return (
      <div className='passport-list'>
        {this.renderCandidates()}
        {this.renderLoader()}
        {this.renderError()}
      </div>
    );
  }

  private getInitialState = () => {
    const candidates = candidateRefs.map((ref): ICandidate => ({
      passportAddress: ref.passportAddress,
      categorizedFacts: null,
      fullName: ref.fullName,
      photo: null,
      socialRating: null,
    }));

    return {
      candidates,
      error: null,
      isLoading: false,
    };
  }

  // #region -------------- Candidates -------------------------------------------------------------------

  private renderCandidates = () => {
    const { candidates } = this.state;

    const columns = candidates.map(candidate => (
      <Col key={candidate.passportAddress} xs={12} sm={6} md={4}>
        <Passport
          onChanged={this.onCandidateChanged}
          candidate={candidate}
          onRequestFactData={this.onRequestFactData}
        />
      </Col>
    ));

    return (
      <Row>
        {columns}
      </Row>
    );
  }

  // #endregion

  // #region -------------- Loader -------------------------------------------------------------------

  private renderLoader = () => {
    const { isLoading } = this.state;

    if (!isLoading) {
      return null;
    }

    return (
      <Loader />
    );
  }

  // #endregion

  // #region -------------- Error -------------------------------------------------------------------

  private renderError = () => {
    const { error } = this.state;

    if (!error) {
      return null;
    }

    return (
      <div className='msg-error'>{error}</div>
    );
  }

  // #endregion

  // #region -------------- Candidates data -------------------------------------------------------------------

  private loadCandidates = async () => {
    this.updateState({ isLoading: true });

    try {
      const { candidates } = this.state;

      for (let index = 0; index < candidates.length; index++) {
        const candidate = candidates[index];

        const updatedCandidate = await loadCandidateData(candidate.passportAddress);
        const updatedCandidates = [...this.state.candidates];

        updatedCandidates[index] = updatedCandidate;
        this.updateState({ candidates: updatedCandidates });
      }
    } catch (err) {
      this.updateState({ error: err.message || err.toString() });
    } finally {
      this.updateState({ isLoading: false });
    }
  }

  private onCandidateChanged = async (candidateAddress: string) => {
    this.updateState({ isLoading: true });

    try {
      const { candidates } = this.state;

      const index = candidates.findIndex(candidate => candidate.passportAddress === candidateAddress);
      if (index === -1) {
        return;
      }

      const updatedCandidate = await loadCandidateData(candidateAddress);

      const updatedCandidates = [...this.state.candidates];
      updatedCandidates[index] = updatedCandidate;
      this.updateState({ candidates: updatedCandidates });
    } catch (err) {
      this.updateState({ error: err.message || err.toString() });
    } finally {
      this.updateState({ isLoading: false });
    }
  }

  private onRequestFactData = async (passportAddress: string, fact: IFact) => {
    const factPayload = await getFactData(passportAddress, fact.key, fact.factProviderAddress);

    Object.assign(fact, factPayload);

    const { candidates } = this.state;
    const index = candidates.findIndex(c => c.passportAddress === passportAddress);
    if (index === -1) {
      return;
    }

    const candidate = candidates[index];
    const updatedCandidate = { ...candidate };

    const updatedCandidates = [...this.state.candidates];

    updatedCandidates[index] = updatedCandidate;
    this.updateState({ candidates: updatedCandidates });
  }

  // #endregion

  // #region -------------- Helpers -------------------------------------------------------------------

  private updateState(updatedProps: Partial<IState>) {
    this.setState({
      ...this.state,
      ...updatedProps,
    });
  }

  // #endregion
}

// #endregion
