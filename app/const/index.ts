export const ETH_NETWORK_URL = 'https://ropsten.infura.io/v3/1f09dda6cce44da68213cacb1ea9bb90';
export const PASSPORT_OWNER_ADDRESS = '0x9275b0Cb4A6B23c2c48B4a72E12B5d73C4f99D01';
export const INFURA_IPFS_API_URL = 'https://ipfs.infura.io:5001/api/v0';
export const EVENT_HASH_STRING_UPDATED = '0x43e6b7e3323b4598401023341c086c07c3ff5577f594b5aab9c065f2c3c9d590';